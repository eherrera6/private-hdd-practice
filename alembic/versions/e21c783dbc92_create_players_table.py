"""create players table

Revision ID: e21c783dbc92
Revises: 
Create Date: 2022-06-27 08:57:39.085009

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'e21c783dbc92'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table("players",
                    sa.Column("id", sa.String(255), primary_key=True),
                    sa.Column("name", sa.String(255)))


def downgrade() -> None:
    op.drop_table("players")
