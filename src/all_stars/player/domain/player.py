from typing_extensions import Self

from src.all_stars.player.domain.player_id import PlayerId
from src.all_stars.player.domain.player_name import PlayerName


class Player:
    id: PlayerId
    name: PlayerName

    def __init__(self, _id: PlayerId,
                 name: PlayerName):
        self.id = _id
        self.name = name

    def __eq__(self, other: Self):
        return all([
            self.id == other.id,
            self.name == other.name
        ])
