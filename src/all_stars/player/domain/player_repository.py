from abc import abstractmethod

from src.all_stars.player.domain.player import Player
from src.all_stars.player.domain.player_id import PlayerId


class PlayerRepository:

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def save(self, player: Player) -> None:
        pass

    @abstractmethod
    def get(self, _id: PlayerId) -> Player:
        pass
