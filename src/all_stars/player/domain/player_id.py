from typing_extensions import Self
import uuid


class PlayerId:
    value: str

    def __init__(self, _id: str):
        self.__ensure_is_valid_uuid(_id=_id)
        self.value = _id

    def __eq__(self, other: Self):
        return all([
            self.value == other.value
        ])

    def __ensure_is_valid_uuid(self, _id: str) -> None:
        uuid.UUID(_id)
