from typing_extensions import Self


class PlayerName:
    value: str

    def __init__(self, name: str):
        self.value = name

    def __eq__(self, other: Self):
        return all([
            self.value == other.value
        ])
