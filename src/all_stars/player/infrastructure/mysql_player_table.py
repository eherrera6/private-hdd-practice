from sqlalchemy import Column, Table
from sqlalchemy import String

from src.all_stars.player.infrastructure.mysql_metadata import MySQLMetadata


class MySQLPlayerTable:
    table: Table

    def __init__(self, mysql_metadata: MySQLMetadata):
        self.table = Table("players", mysql_metadata.metadata,
                           Column("id", String(255), primary_key=True),
                           Column("name", String(255)))
