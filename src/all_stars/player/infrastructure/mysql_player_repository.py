from typing import Optional

from sqlalchemy import Table, select
from sqlalchemy.engine import Engine

from src.all_stars.player.domain.player import Player
from src.all_stars.player.domain.player_id import PlayerId
from src.all_stars.player.domain.player_name import PlayerName
from src.all_stars.player.domain.player_repository import PlayerRepository
from src.all_stars.player.infrastructure.mysql_engine import MySQLEngine
from src.all_stars.player.infrastructure.mysql_player_table import MySQLPlayerTable


class MySQLPlayerRepository(PlayerRepository):
    def __init__(self, mysql_engine: MySQLEngine,
                 mysql_table: MySQLPlayerTable):
        self.__engine: Engine = mysql_engine.engine
        self.__table: Table = mysql_table.table

    def __execute_sql(self, sql):
        with self.__engine.connect() as connection:
            data = connection.execute(sql)

        return data

    def __entity_to_row(self, entity: Player) -> dict:
        return dict(
            id=entity.id.value,
            name=entity.name.value
        )

    def __row_to_entity(self, row) -> Player:
        return Player(
            _id=PlayerId(row.id),
            name=PlayerName(row.name)
        )

    def save(self, player: Player) -> None:
        args = self.__entity_to_row(entity=player)
        if self.get(_id=player.id):
            sql = self.__table.update().where(self.__table.c.id == player.id.value).values(args)
        else:
            sql = self.__table.insert().values(args)
        self.__execute_sql(sql=sql)

    def get(self, _id: PlayerId) -> Optional[Player]:
        sql = select([self.__table]).where(self.__table.c.id == _id.value)
        row = self.__execute_sql(sql=sql).first()
        if row:
            return self.__row_to_entity(row=row)
