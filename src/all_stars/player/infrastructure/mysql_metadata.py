from sqlalchemy import MetaData


class MySQLMetadata:
    metadata: MetaData

    def __init__(self):
        self.metadata = MetaData()