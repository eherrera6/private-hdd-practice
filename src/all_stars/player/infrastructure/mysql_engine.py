import os

from sqlalchemy import create_engine
from sqlalchemy.engine import Engine


class MySQLEngine:
    engine: Engine

    def __init__(self):
        database_user = os.getenv("DATABASE_USER")
        database_passwd = os.getenv("DATABASE_PASSWORD")
        database_host = os.getenv("DATABASE_HOST")
        database_name = os.getenv("DATABASE_NAME")

        url = f"mysql+pymysql://{database_user}:{database_passwd}@{database_host}/{database_name}"
        self.engine = create_engine(url=url)
