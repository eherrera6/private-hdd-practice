from src.all_stars.player.domain.player import Player
from src.all_stars.player.domain.player_id import PlayerId
from src.all_stars.player.domain.player_name import PlayerName
from src.all_stars.player.domain.player_repository import PlayerRepository


class PlayerCreator:
    def __init__(self, player_repository: PlayerRepository):
        self.__player_repository = player_repository

    def __call__(self, _id: str,
                 name: str) -> None:

        player = Player(_id=PlayerId(_id=_id),
                        name=PlayerName(name=name))

        self.__player_repository.save(player=player)
