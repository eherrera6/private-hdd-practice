from fastapi import FastAPI
import uvicorn

from app.all_stars.container import Container as AllStarsContainer
from app.all_stars.routers.players.router import get_router as players_get_router


def get_api() -> FastAPI:
    app = FastAPI(
        title="All-Stars API",
        description="API for storing the results of our office matches"
    )

    players_router = players_get_router()

    app.include_router(router=players_router)

    return app


if __name__ == "__main__":
    AllStarsContainer()
    app = get_api()
    uvicorn.run(app=app,
                host="0.0.0.0",
                port=5000)
