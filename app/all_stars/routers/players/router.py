from fastapi import APIRouter

from app.all_stars.routers.players.put_player import put_player


def get_router() -> APIRouter:
    router = APIRouter(
        prefix="/players",
        tags=["players"]
    )
    router.add_api_route(
        path="/{player_id}",
        endpoint=put_player,
        methods=["PUT"]
    )

    return router
