from dependency_injector.wiring import Provide, inject
from fastapi import Depends, Request
from fastapi.responses import Response
from starlette.status import HTTP_201_CREATED

from app.all_stars.container import Container
from src.all_stars.player.application.create.player_creator import PlayerCreator


@inject
async def put_player(request: Request,
                     player_id: str,
                     player_creator: PlayerCreator = Depends(Provide[Container.player_creator])) -> Response:
    request_body = await request.json()
    player_name = request_body["name"]

    player_creator(_id=player_id,
                   name=player_name)

    return Response(status_code=HTTP_201_CREATED)
