from dependency_injector import containers, providers

from src.all_stars.player.application.create.player_creator import PlayerCreator
from src.all_stars.player.infrastructure.mysql_engine import MySQLEngine
from src.all_stars.player.infrastructure.mysql_metadata import MySQLMetadata
from src.all_stars.player.infrastructure.mysql_player_repository import MySQLPlayerRepository
from src.all_stars.player.infrastructure.mysql_player_table import MySQLPlayerTable


class Container(containers.DeclarativeContainer):
    wiring_config = containers.WiringConfiguration(packages=[".routers"])

    engine = providers.Singleton(MySQLEngine)
    metadata = providers.Singleton(MySQLMetadata)

    player_table = providers.Singleton(MySQLPlayerTable,
                                       mysql_metadata=metadata)
    player_repository = providers.Singleton(MySQLPlayerRepository,
                                            mysql_engine=engine,
                                            mysql_table=player_table)
    player_creator = providers.Factory(PlayerCreator, player_repository=player_repository)
