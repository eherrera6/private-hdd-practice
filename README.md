# ALL-STARS API

Objetivos:

* Crear jugadores.
* Conseguir el listado de jugadores.
* Crear partidos.
	* Cada partido puede tener varios jugadores.
	* Para cada jugador, indicar el numero de puntos conseguidos.
* Modificar partidos.
	* Incorporar nuevos jugadores.
	* Modificar los puntos de los jugadores.


## Dominio

* Jugador: Tendra un identificador y un nombre.
* Partido: Tendrá un identificador y la fecha. Un partido estara compuesto por uno o varios jugadores, cada uno con el numero de puntos conseguidos


## Stack

* Python
* Docker para tener un MySQL local
* SQLAlchemy como controlador
* Alembic para ir actualizando la base de datos
* Pytest para los tests
* Pipenv para instalar las dependencias
* 