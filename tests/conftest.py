from dotenv import load_dotenv

import pytest


@pytest.fixture(scope="session")
def load_env() -> None:
    # Added for being able to debug the tests
    load_dotenv()
