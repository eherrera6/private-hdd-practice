from typing import Optional

from src.all_stars.player.domain.player import Player
from src.all_stars.player.domain.player_id import PlayerId
from src.all_stars.player.domain.player_name import PlayerName
from tests.all_stars.player.domain.player_id_mother import a_player_id
from tests.all_stars.player.domain.player_name_mother import a_player_name


def a_player(_id: Optional[PlayerId] = None,
             name: Optional[PlayerName] = None) -> Player:
    if _id is None:
        _id = a_player_id()

    if name is None:
        name = a_player_name()

    return Player(_id=_id,
                  name=name)
