from typing import Optional
import uuid

from src.all_stars.player.domain.player_name import PlayerName


def a_player_name(name: Optional[str] = None) -> PlayerName:
    if name is None:
        name = str(uuid.uuid4())

    return PlayerName(name=name)
