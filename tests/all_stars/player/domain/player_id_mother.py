from typing import Optional
import uuid

from src.all_stars.player.domain.player_id import PlayerId


def a_player_id(_id: Optional[str] = None) -> PlayerId:
    if _id is None:
        _id = str(uuid.uuid4())

    return PlayerId(_id=_id)
