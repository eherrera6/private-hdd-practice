import contextlib

from fastapi.testclient import TestClient
import pytest

from app.api import get_api
from app.all_stars.container import Container
from src.all_stars.player.infrastructure.mysql_engine import MySQLEngine
from src.all_stars.player.infrastructure.mysql_metadata import MySQLMetadata
from src.all_stars.player.infrastructure.mysql_player_repository import MySQLPlayerRepository
from src.all_stars.player.infrastructure.mysql_player_table import MySQLPlayerTable


@pytest.fixture(scope="module")
def container(load_env) -> Container:
    container = Container()
    yield container


@pytest.fixture(scope="module")
def api_client():
    app = get_api()
    yield TestClient(app=app)


@pytest.fixture(scope="module")
def mysql_engine(container) -> MySQLEngine:
    return container.engine()


@pytest.fixture(scope="module")
def mysql_metadata(container) -> MySQLMetadata:
    return container.metadata()


@pytest.fixture(scope="module")
def mysql_players_table(container) -> MySQLPlayerTable:
    return container.player_table()


@pytest.fixture(autouse=True)
def clean_database(mysql_engine, mysql_metadata) -> None:
    with contextlib.closing(mysql_engine.engine.connect()) as con:
        trans = con.begin()
        for table in reversed(mysql_metadata.metadata.sorted_tables):
            con.execute(table.delete())
        trans.commit()


@pytest.fixture()
def mysql_player_repository(container) -> MySQLPlayerRepository:
    return container.player_repository()
