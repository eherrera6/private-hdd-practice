import json

from tests.all_stars.player.domain.player_mother import a_player
from tests.all_stars.player.domain.player_name_mother import a_player_name


def test_put_a_player_no_error_raised(api_client):
    player = a_player()
    body = {
        "name": player.name.value
    }

    response = api_client.put(f"/players/{player.id.value}",
                              data=json.dumps(body))

    assert response.status_code == 201


def test_player_saved_it_is_persisted(api_client, mysql_player_repository):
    player = a_player()
    body = {
        "name": player.name.value
    }

    response = api_client.put(f"/players/{player.id.value}",
                              data=json.dumps(body))

    assert response.status_code == 201
    saved_player = mysql_player_repository.get(_id=player.id)
    assert saved_player == player


def test_when_a_player_with_the_id_exists_then_it_is_updated(api_client, mysql_player_repository):
    player = a_player(
        name=a_player_name(name="John Cena")
    )
    mysql_player_repository.save(player=player)
    body = {
        "name": "Randy Orton"
    }

    response = api_client.put(f"/players/{player.id.value}",
                              data=json.dumps(body))

    assert response.status_code == 201
    saved_player = mysql_player_repository.get(_id=player.id)
    assert saved_player != player
    assert saved_player.name.value == "Randy Orton"
