import contextlib

import pytest

from src.all_stars.player.infrastructure.mysql_engine import MySQLEngine
from src.all_stars.player.infrastructure.mysql_metadata import MySQLMetadata
from src.all_stars.player.infrastructure.mysql_player_repository import MySQLPlayerRepository
from src.all_stars.player.infrastructure.mysql_player_table import MySQLPlayerTable


@pytest.fixture(scope="module")
def mysql_engine(load_env) -> MySQLEngine:
    return MySQLEngine()


@pytest.fixture(scope="module")
def mysql_metadata() -> MySQLMetadata:
    return MySQLMetadata()


@pytest.fixture(scope="module")
def mysql_players_table(mysql_metadata) -> MySQLPlayerTable:
    return MySQLPlayerTable(mysql_metadata=mysql_metadata)


@pytest.fixture(autouse=True)
def clean_database(mysql_engine, mysql_metadata) -> None:
    with contextlib.closing(mysql_engine.engine.connect()) as con:
        trans = con.begin()
        for table in reversed(mysql_metadata.metadata.sorted_tables):
            con.execute(table.delete())
        trans.commit()


@pytest.fixture()
def mysql_player_repository(mysql_engine, mysql_players_table) -> MySQLPlayerRepository:
    return MySQLPlayerRepository(
        mysql_engine=mysql_engine,
        mysql_table=mysql_players_table
    )
