from tests.all_stars.player.domain.player_mother import a_player
from tests.all_stars.player.domain.player_name_mother import a_player_name


def test_player_saved_no_error_raised(mysql_player_repository):
    player = a_player()

    mysql_player_repository.save(player=player)


def test_player_saved_it_is_persisted(mysql_player_repository):
    player = a_player()

    mysql_player_repository.save(player=player)
    saved_player = mysql_player_repository.get(_id=player.id)

    assert saved_player == player


def test_when_a_player_with_the_id_exists_then_it_is_updated(mysql_player_repository):
    player = a_player(
        name=a_player_name(name="John Cena")
    )
    mysql_player_repository.save(player=player)

    same_player = a_player(
        _id=player.id,
        name=a_player_name(name="Randy Orton")
    )
    mysql_player_repository.save(player=same_player)

    saved_player = mysql_player_repository.get(_id=player.id)
    assert saved_player != player
    assert saved_player.name.value == "Randy Orton"
