from tests.all_stars.player.domain.player_mother import a_player


def test_player_created_without_errors(player_creator):
    player = a_player()

    player_creator(_id=player.id.value,
                   name=player.name.value)


def test_player_created_then_player_repository_called(player_creator, player_repository):
    player = a_player()

    player_creator(_id=player.id.value,
                   name=player.name.value)

    player_repository.save.assert_called()


def test_player_exists_then_repository_called_nonetheless(player_creator, player_repository):
    player = a_player()
    player_repository.get = lambda _id: player

    player_creator(_id=player.id.value,
                   name=player.name.value)

    player_repository.save.assert_called()
