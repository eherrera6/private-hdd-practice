from unittest.mock import Mock

import pytest

from src.all_stars.player.application.create.player_creator import PlayerCreator
from src.all_stars.player.domain.player_repository import PlayerRepository


# Infrastructure
@pytest.fixture()
def player_repository() -> Mock:
    return Mock(spec=PlayerRepository)


# Applications
@pytest.fixture()
def player_creator(player_repository) -> PlayerCreator:
    return PlayerCreator(player_repository=player_repository)
